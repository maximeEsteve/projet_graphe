import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class TestUnitaire 
{
	/**
     * Test de la construction d'un graphe par d�faut
     */
	@Test
	public void testConstructionGrapheParDefault() 
	{
		Graphe graph=new Graphe();
		//assertEquals("Document Vide",graph.getNom());
		//assertEquals("",graph.getFichier());
		assertEquals(3,graph.getEpaisseurDefaut());
		assertEquals(Color.black,graph.getCouleurDefaut());
		assertEquals(0,graph.getSommets().size());
		assertEquals(0,graph.getAretes().size());
	}
	/**
     * Test de la construction d'un sommet � partir d'un graphe
     * existant.
     */
	@Test
	public void testConstructionSommetGraphe()
	{
		Graphe graph=new Graphe();
		int x=10;
		int y=20;
		graph.ajoutSommet(x,y);
		Sommet som=graph.getSommets().getFirst();
		assertEquals(graph.getCouleurDefaut(),som.getCouleur());
		assertEquals(graph.getEpaisseurDefaut(),som.getEpaisseur());
		assertEquals(graph.getTailleDefaut(),som.getTaille());
		
		assertEquals(x-graph.getTailleDefaut()/2,som.getPosX());
		assertEquals(y-graph.getTailleDefaut()/2,som.getPosY());
	}
	/**
     * Test de la construction d'une ar�te � partir d'un graphe
     * existant.
     */
	@Test
	public void testConstructionArreteGraphe() 
	{
		Graphe graph=new Graphe();
		int x_PremierSommet=20;
		int y_PremierSommet=30;
		
		int x_DeuxiemeSommet=50;
		int y_DeuxiemeSommet=50;
		
		graph.ajoutSommet(x_PremierSommet,y_PremierSommet);
		graph.ajoutSommet(x_DeuxiemeSommet,y_DeuxiemeSommet);
		
		Sommet sour=graph.getSommets().getFirst();
		Sommet dest=graph.getSommets().getLast();
		graph.ajoutArete(sour,dest);
		Arete aret=graph.getAretes().getFirst();
		
		assertEquals(aret.getCouleur(),graph.getCouleurDefaut());
		assertEquals(aret.getEpaisseur(),graph.getEpaisseurDefaut());
		assertEquals(sour,aret.getSource());
		assertEquals(dest,aret.getDestination());	
	}
	/**
     * Test du d�placement d'un sommet
     */
	@Test
	public void testDeplacemementSommet() 
	{
		Graphe graph=new Graphe();
		graph.ajoutSommet(40,30);
		graph.getSommets().getFirst().move(200,300);
		assertEquals(graph.getSommets().getFirst().getPosX(),200);
		assertEquals(graph.getSommets().getFirst().getPosY(),300);
	}
	

}