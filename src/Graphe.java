
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.LinkedList;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.Math;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;



import sun.awt.AWTAccessor.InputEventAccessor;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 *
 * <p>
 *  Graphe repr�sente une zone de dessin (un JPanel)
 *  sur laquelle on peut dessiner des sommets
 *  qui peuvent prendre la forme de  :
 * <ul>
 * 		<li>rectangles</li> 
 * 		<li>ronds</li>
 * 		<li>triangles</li>
 * </ul>
 * </p>
 * <p>
 * Des ar�tes peuvent lier deux sommets entre eux. 
 * 
 * Le graphe poss�de un nom et peut �tre export�
 * sous l'extension .dot .
 * 
 * A partir du graphe on peut choisir la couleur,
 * la taille et l'�paisseur des futurs sommets et ar�tes
 * 
 * </p>
 * @author Manu Patrois
 * @version 1.0
 * 
 */
public class Graphe extends JPanel implements MouseListener,MouseMotionListener
{

	private Point pointMoveOrigine=null;
	private Point pointClicDroit=null;

	private static final long serialVersionUID = 6011583867430456365L;
	/**
	 * Le graphe sera enregistr� sous ce nom 
	 * de fichier
	 */
	private String nomFichier;

	/**
	 * La couleur des futurs sommets et ar�tes
	 */
	private Color couleurDefaut;

	/**
	 * L'�paisseur des futurs sommets et ar�tes
	 */
	private int epaisseurDefaut;

	/**
	 * La taille des futurs sommets et ar�tes
	 */
	private int tailleDefaut;

	/**
	 * La liste des sommets du graphe
	 */
	private LinkedList<Sommet> sommets;

	/**
	 * La liste des ar�tes du graphe
	 */
	private LinkedList<Arete> aretes;

	/**
	 * Le zoom du graphe
	 */
	private float zoom=1;

	/**
	 * L'origine d'affichage sur la zone de dessin
	 */
	private Point origine;

	/**
	 * Le dernier sommet sur lequel on a cliqu�
	 */
	private Sommet somTmp=null;
	/**
	 * La forme des futurs sommets
	 */
	private String typeSommet;

	/**
	 * Le point de d�part d'une selection
	 */
	private Point mouseAnchor;

	/**
	 * Le rectangle de selection
	 */
	private Selection selection=null;

	/**
	 * Le rectangle de selection
	 */
	private Point endLine=null;

	/**
	 * Si on est en mode d�placement ou cr�ation
	 */
	private boolean modeDeplacement=false;

	/**
	 * Ceci est une classe interne qui permet la cr�ation
	 * et l'affichage d'un rectangle de s�l�ction
	 */

	public class Selection
	{

		private LinkedList<Sommet> sommetSelect=new LinkedList<Sommet>();
		private LinkedList<Arete> areteSelect=new LinkedList<Arete>();

		private LinkedList<Sommet> sommetCopier=new LinkedList<Sommet>();
		private LinkedList<Arete> areteCopier=new LinkedList<Arete>();

		public int x,y,width,height;
		boolean visible;
		private Object getSource;
		Selection()
		{
			x = 0;
			y = 0;
			width = 0;
			height = 0;
			visible=false;
		}
		void setRectSelect(Point p1,Point p2)
		{
			x = Math.min(p1.x, p2.x);
			y = Math.min(p1.y, p2.y);
			width = Math.max(p1.x - p2.x, p2.x - p1.x);
			height = Math.max(p1.y - p2.y, p2.y - p1.y);
			visible=false;
		}
		public boolean containMouse(int coordX, int coordY)
		{
			if (coordX>x && coordX < (x+width)  &&
					coordY>x && coordY < (y+height) )
				return true;
			else 
				return false;	
		}
		public void paint(Graphics g)
		{
			if(visible)
			{
				g.setColor(Color.green);
				g.drawRect((int)(x*zoom+origine.x*zoom),
						(int)(y*zoom+origine.y*zoom),
						(int)(width*zoom),
						(int)(height*zoom));
				g.setColor(couleurDefaut);
			}
			for (Sommet s : sommetSelect) 
			{
				s.tracerSommet(g,getZoom(),getOrigine());
			}
			for (Arete a : areteSelect) 
			{
				a.tracerArete(g,getZoom(),getOrigine());
			}
		}
		public void addSommet(Sommet s)
		{
			if(!sommetSelect.contains(s))
			{
				sommetSelect.add(s);
			}	
		}
		public void addArete(Arete a)
		{
			if(!areteSelect.contains(a))
			{
				areteSelect.add(a);
			}
		}
		public void removeSommet(Sommet s)
		{
			sommetSelect.remove(s);
		}
		public void removeArete(Arete a)
		{
			areteSelect.remove(a);
		}
		public boolean sommetOuAreteSelectionne()
		{
			return (sommetSelect.size()>0 || areteSelect.size()>0);
		}
		public void moveSelection(int dx,int dy)
		{
			for (Sommet s : sommetSelect) 
			{
				s.move(s.getPosX()+dx,s.getPosY()+dy);
			}
		}
		void retabliNonSelectionne()
		{
			for (Arete a : aretes) {
				a.setSelected(false);
			}
			for (Sommet s : sommets) {
				s.setSelection(false);
			}
			selection.sommetSelect.clear();
			selection.areteSelect.clear();
			selection.visible=false;
		}
		void copier()
		{
			sommetCopier.clear();
			areteCopier.clear();
			for (Arete a : areteSelect)
			{
				areteCopier.add(a);
			}
			for (Sommet s : sommetSelect)
			{
				sommetCopier.add(s);
			}
			retabliNonSelectionne();
			repaint();
		}
		void couper()
		{
			sommetCopier.clear();
			areteCopier.clear();
			for (Arete a : areteSelect)
			{
				areteCopier.add(a);
				aretes.remove(a);
			}
			for (Sommet s : sommetSelect)
			{
				sommetCopier.add(s);
				sommets.remove(s);
			}
			
			retabliNonSelectionne();
			repaint();
		}
		public Sommet existeDeja(Sommet som,LinkedList<Sommet> list)
		{
			for (Sommet s : list) {
				if(s.pareil(som))
					return s;
			}
			return null;
		}
		void coller(int x,int y)
		{
			if(sommetCopier.size()>0)
			{
				Point ptTmp=pointHautDroiteCopier();
				LinkedList<Sommet> SommetNePasCopier=new LinkedList<Sommet>();
				LinkedList<Sommet> SommetDejaColler=new LinkedList<Sommet>();

				for (Arete a : areteCopier) 
				{
					System.out.println(areteCopier.size());
					if(sommetCopier.contains(a.getSource()) && sommetCopier.contains(a.getDestination()) )
					{
							SommetNePasCopier.add(a.getSource());
							SommetNePasCopier.add(a.getDestination());
							
							
							Sommet sour=new Sommet(a.getSource());
							Sommet des=new Sommet(a.getDestination());
							
							des.move(x-(ptTmp.x-des.getPosX()),y-(ptTmp.y-des.getPosY()));
							sour.move(x-(ptTmp.x-sour.getPosX()),y-(ptTmp.y-sour.getPosY()));
							
							//des.setPosX(x-(ptTmp.x-des.getPosX()));
//							des.setPosY(y-(ptTmp.y-des.getPosY()));
//							sour.setPosX(x-(ptTmp.x-sour.getPosX()));
//							sour.setPosY(y-(ptTmp.y-sour.getPosY()));
							
							if(existeDeja(sour,SommetDejaColler)!=null)
							{
								sour=existeDeja(sour,SommetDejaColler);
							}
							else
							{
								SommetDejaColler.add(sour);
							}
							if(existeDeja(des,SommetDejaColler)!=null)
							{
								des=existeDeja(des,SommetDejaColler);
							}
							else
							{
								SommetDejaColler.add(des);
							}

							Arete tmp=new Arete(a,sour,des);
							
							aretes.add(tmp);
							sommets.add(sour);
							sommets.add(des);
					}
				}
				for (Sommet sommet : SommetNePasCopier) {
					sommetCopier.remove(sommet);
				}
				for (Sommet s : sommetCopier) 
				{
					int posx=s.getPosX();
					int posy=s.getPosY();

					int disOriX=ptTmp.x-posx;
					int disOriY=ptTmp.y-posy;

					Sommet som=new Sommet(s);
					som.move(x-disOriX, y-disOriY);
					
					sommets.add(som);
				}
				for (Sommet sommet : SommetNePasCopier) {
					sommetCopier.add(sommet);
				}

				repaint();
			}


		}
//		void copier()
//		{
//			sommetCopier.clear();
//			areteCopier.clear();
//			for (Arete a : areteSelect)
//			{
//				if(sommetSelect.contains(a.getSource()) && sommetSelect.contains(a.getDestination()) )
//				{
//					sommetSelect.remove(a.getSource());
//					sommetSelect.remove(a.getDestination());
//					
//					Sommet sour=new Sommet(a.getSource());
//					Sommet des=new Sommet(a.getDestination());
//					Arete tmp=new Arete(a,sour,des);
//					areteCopier.add(tmp);
//					sommetCopier.add(sour);
//					sommetCopier.add(des);
//				}
//			}
//			for (Sommet s : sommetSelect)
//			{
//				sommetCopier.add(new Sommet(s));
//			}
//			retabliNonSelectionne();
//			repaint();
//		}
//		void coller(int x,int y)
//		{
//			if(sommetCopier.size()>0)
//			{
//				Point ptTmp=pointHautDroiteCopier();
//
//				for (Arete a : areteCopier) 
//				{
//					aretes.add(a);	
//				}
//
//				for (Sommet s : sommetCopier) 
//				{
//					int posx=s.getPosX();
//					int posy=s.getPosY();
//
//					int disOriX=ptTmp.x-posx;
//					int disOriY=ptTmp.y-posy;
//
//					s.setPosX(x-disOriX);
//					s.setPosY(y-disOriY);
//					
//					sommets.add(s);
////					s.setPosX(posx);
////					s.setPosY(posy); 
//				}
//
//				repaint();
//			}
//
//
//		}
		Point pointHautDroiteCopier()
		{
			int minX=0;
			int minY=0;
			if(sommetCopier.size()>0)
			{
				minX=sommetCopier.getFirst().getPosX();
				minY=sommetCopier.getFirst().getPosY();
			}
			//Point pHD=new Point(minY,minY);
			for (Sommet s : sommetCopier) 
			{
				if(s.getPosX()<minX)
					minX=s.getPosX();
				if(s.getPosY()<minY)
					minY=s.getPosY();
			}
			return new Point(minX,minY);
		}
	}
	/**
	 * Constructeur par d�faut de la classe Graphe
	 * Cette m�thode est appel�e lors de la cr�ation de la fen�tre
	 * de l'application. Elle d�rive de JPanel et impl�mente MouseListener 
	 * et MouseMotionListener.
	 * Par d�faut le nom du graphe pour la sauvegarde est "Document vide"
	 * la couleur des futurs sommets et ar�tes noire, l'�paisseur est de 1
	 * la taille par d�faut 20 et il n' y a aucun sommet et aucune ar�te
	 */

	JPopupMenu popupMenu = new JPopupMenu();
	JMenuItem mntmCouper = new JMenuItem("Couper");
	JMenuItem mntmCopier = new JMenuItem("Copier");
	JMenuItem mntmColler = new JMenuItem("Coller");
	JSeparator separator = new JSeparator();
	JMenuItem mntmSupprimer = new JMenuItem("Supprimer");

	Graphe()
	{
		super();
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.setBackground(Color.white);
		this.nomFichier="Document Vide";
		this.typeSommet="Rectangle";
		couleurDefaut=Color.black;
		epaisseurDefaut=1;
		setTailleDefaut(20);
		sommets =new LinkedList<Sommet>();
		aretes =new LinkedList<Arete>();
		origine=new Point(0,0);
		typeSommet="Rectangle";
		selection=new Selection();
		mntmCouper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection.couper();
			}
		});


		popupMenu.add(mntmCouper);
		mntmCopier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(selection.sommetOuAreteSelectionne())
				{
					selection.copier();
				}
			}
		});


		popupMenu.add(mntmCopier);
		mntmColler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection.coller(pointClicDroit.x, pointClicDroit.y);
			}
		});


		popupMenu.add(mntmColler);


		popupMenu.add(separator);
		mntmSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				supprimerElementsSelectionnes();
			}
		});


		popupMenu.add(mntmSupprimer);

		JMenuItem mntmModifer = new JMenuItem("Modifer");
		mntmModifer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if(selection.sommetSelect.size()>0 || selection.areteSelect.size()>0 )
				{
					FenetrePropriete fp=new FenetrePropriete(selection.sommetSelect,selection.areteSelect);
					fp.setModal(true);
					fp.showDialog();

					selection.retabliNonSelectionne();
					repaint();

				}
			}
		});
		popupMenu.add(mntmModifer);

	}
	/**
	 * Ajoute un sommet en fonction de x et y,de la couleur actuelle du graphe
	 * de l'�paisseur du trait et de la taille par d�faut et du type de sommet
	 * (rectangle rond ou carr� )
	 * @param x
	 * La position du futur sommet en x
	 * @param y
	 * La position du futur sommet en y	
	 */
	void ajoutSommet(int x,int y)
	{
		sommets.add(new Sommet(couleurDefaut,epaisseurDefaut,x-tailleDefaut/2,y-tailleDefaut/2,
				getTailleDefaut(),typeSommet));
	}

	/**
	 * Ajoute une ar�te en fonction d'un sommet source et d'un sommet destination,
	 * de la couleur actuelle du graphe et 
	 * de l'�paisseur du trait et de la taille par d�faut
	 *  @param sour
	 * Le sommet source
	 * @param dest
	 * Le sommet destination
	 */
	void ajoutArete(Sommet sour,Sommet dest)
	{
		aretes.add(new Arete(couleurDefaut,epaisseurDefaut,sour,dest));
	}
	void chargerGraphe()
	{
		//TODO
	}

	/**
	 *Dessine tous les �l�ments situ�s sur la zone de dessin
	 *  @param g
	 */
	public void paint(Graphics g)
	{
		super.paint(g);
		/* Dessine les sommets*/
		for (Sommet s : this.getSommets()) 
		{
			if(!s.isSelected())
				s.tracerSommet(g,getZoom(),getOrigine());
		}
		/* Dessine les ar�tes*/
		for (Arete a : this.getAretes()) 
		{
			if(!a.isSelected())
				a.tracerArete(g,getZoom(),getOrigine());
		}
		/* Dessine le rectangle de selection si il est visible*/

		selection.paint(g);

		if(somTmp!=null && endLine!=null)
		{
			g.setColor(couleurDefaut);
			g.drawLine((int)(somTmp.centreSommet().x*zoom+origine.x*zoom),
					(int)(somTmp.centreSommet().y*zoom+origine.y*zoom), 
					(int)(endLine.x),
					(int)(endLine.y));
		}
	}
	void appliquerAlgorithme()
	{

	}
	void supprimerArete()
	{

	}
	void supprimerNoeud()
	{

	}
	void selectionner()
	{

	}

	/**
	 *Cette m�thode est appel�e lors d'un clic
	 *Elle entraine la cr�ation d'un sommet
	 *  @param arg0
	 *  	L'evenement de la souris
	 */
	@Override
	public void mouseClicked(MouseEvent arg0) 
	{
		/* Si le clic est celui du bouton gauche*/
		if(SwingUtilities.isLeftMouseButton(arg0))
		{
			Sommet tmp=this.SommetClicked(arg0);
			/* Si on clic sur aucun sommet
			  et qu'aucune selection n'est en cours
			  on ajoute un sommet */
			if(tmp==null  && !selection.sommetOuAreteSelectionne())
			{
				this.ajoutSommet(pointSouris(arg0).x, pointSouris(arg0).y);
			}

			/* Si on clic sur aucun sommet
			  on alors on annule toute selection
			 */
			if(somTmp==null)
			{
				selection.retabliNonSelectionne();
				repaint();
			}
		}
		/* Si le clic est celui du bouton droit*/
		else if(SwingUtilities.isRightMouseButton(arg0))
		{
			//pointClicDroit=arg0.getPoint();
			pointClicDroit=pointSouris(arg0);
			Sommet tmp=this.SommetClicked(arg0);
			//Si on clic droit sur un sommet on le selectionne
			if(tmp!=null)
			{
				tmp.setSelection(true);
				selection.addSommet(tmp);
			}
			//On lance le menu popup du clic droit
			if(!selection.sommetOuAreteSelectionne())
			{
				mntmCopier.setEnabled(false);
				mntmCouper.setEnabled(false);
			}
			else
			{
				mntmCopier.setEnabled(true);
				mntmCouper.setEnabled(true);
				
				
			}
			if(selection.sommetCopier.size()==0)
				mntmColler.setEnabled(false);
			else
				mntmColler.setEnabled(true);
			popupMenu.show(arg0.getComponent(), arg0.getX(), arg0.getY());
			repaint();
		}
	}
	/**
	 *Cette m�thode est appel�e lorsque les bouttons de la souris
     sont press�s
	 *  @param e
	 *  	L'evenement de la souris
	 */
	@Override
	public void mousePressed(MouseEvent e) 
	{

		/* Si le bouton press� est celui de gauche*/
		if(SwingUtilities.isLeftMouseButton(e))
		{
			/*D�finition du point d'ancrage de la souris
			 pour une �ventuelle s�lection
			 */
			mouseAnchor=pointSouris(e);
			pointMoveOrigine=e.getPoint();

			somTmp = SommetClicked(e);

			/* Si on clic sur sommet ...*/
			if(somTmp!=null)
			{
				/* ...et que la touche ctrl est enfonc�e ...*/
				if ((e.getModifiers() & InputEvent.CTRL_MASK) == InputEvent.CTRL_MASK)
				{
					/* ...alors le sommet sur lequel on a cliqu� est s�lectionn�*/
					somTmp.setSelection(!somTmp.isSelected());
					selection.addSommet(somTmp);
					repaint();
				}
			}
		}
	}
	/**
	 *Cette méthode est appelée lorsque les bouttons de la souris
     sont relachés
	 *  @param e
	 *  	L'evenement de la souris
	 */
	@Override
	public void mouseReleased(MouseEvent e) 
	{
		/* Si le bouton relaché est celui de gauche*/
		if(SwingUtilities.isLeftMouseButton(e))
		{

			Sommet tmp=this.SommetClicked(e);
			/* Si un sommet source (somTmp) et un sommet destination (tmp) 
			 sont définis et visibles*/
			if(somTmp!=null && tmp!=null && somTmp!=tmp && somTmp.getVisible()==true && tmp.getVisible()==true)
			{
				/* On ajoute un ar�te reliant les deux sommets si il ny en pas deja une*/
				if(areteNonExistante(somTmp,tmp) && !modeDeplacement)
				{
					this.ajoutArete(somTmp, tmp);
				}
			}
			/* on met la ligne de trait a null et la rectangle de selection invisible*/
			endLine=null;
			selection.visible=false;
		}
		repaint();
	}

	private boolean areteNonExistante(Sommet somTmp2, Sommet tmp) 
	{
		for (Arete arete : aretes) 
		{
			if( (arete.getSource().equals(somTmp2) && arete.getDestination().equals(tmp))
					|| 	(arete.getSource().equals(tmp) && arete.getDestination().equals(somTmp)) )
				return false;
		}
		return true;
	}
	@Override
	public void mouseDragged(MouseEvent arg0) 
	{
		/* Si le drag est celui du bouton gauche */
		if(SwingUtilities.isLeftMouseButton(arg0))
		{
			/* Si on navigue et déplace l'origine en maintenant alt */
			if (((arg0.getModifiers() & InputEvent.ALT_MASK) == InputEvent.ALT_MASK))
			{

				/*On calcul la distance parcourue depuis le dernier déplacement
				 * de la souris et on le met dans dx et dy*/
				int dx=arg0.getX()-pointMoveOrigine.x;
				int dy=arg0.getY()-pointMoveOrigine.y;

				/*On change le dernier endroit ou on a fait un drag de souris 
				 *et on le remplace par la position actuelle de la souris */
				pointMoveOrigine=arg0.getPoint();
				//selection.moveSelection(pointMoveOrigine.x, pointMoveOrigine.y);

				/*On affecte la nouvelle origine du dessin en fonction de la distance 
				 * parcourue depuis le dernier deplacement */
				//if(!modeDeplacement)
				origine=new Point((int)(origine.x+(dx/zoom)),(int)(origine.y+(dy/zoom)));
				//else


				repaint();
			}
			else
			{


				/* Si on est sur un sommet et en mode deplacement*/
				if(somTmp!=null && modeDeplacement)
				{
					if(selection.sommetOuAreteSelectionne())
					{
						int dx=arg0.getX()-pointMoveOrigine.x;
						int dy=arg0.getY()-pointMoveOrigine.y;
						pointMoveOrigine=arg0.getPoint();
						selection.moveSelection((int)(dx/zoom),(int)(dy/zoom));
					}
					else	
						somTmp.move(pointSouris(arg0).x-somTmp.getTaille()/2,pointSouris(arg0).y-somTmp.getTaille()/2);
					repaint();
				}
				/* Sinon on déplace les coordonnée du point destination de l'arête tmp*/
				else if (somTmp!=null && !modeDeplacement)
				{
					endLine=new Point(arg0.getX(),arg0.getY());
					repaint();
				}

				/* Si le point d'ancrage de la souris est défini et qu'on a pas cliqué sur un sommet
				 * alors on dessine un rectangle de selection
				 */
				if(mouseAnchor!=null && somTmp==null)
				{
					/* Le rectangle de selection se créé en fonction du point d'ancrage
					 * de la souris et du point "dragged" de la souris
					 */
					selection.setRectSelect(mouseAnchor,pointSouris(arg0));
					selection.visible=true;

					/* On parcours la liste de sommet*/
					for (Sommet s : sommets) 
					{
						/* Si un sommet touche le rectangle de selection alors il devient sélectionné..*/
						if(s.InRectangle(selection.x,selection.y,selection.width,selection.height)  && s.getVisible())
						{
							s.setSelection(true);
							/*Et on l'ajoute à la selection */
							selection.addSommet(s);	
						}
						else
						{
							/* Sinon il devient déselectionné sauf si la touhe ctrl est enfoncée.. */
							if (!((arg0.getModifiers() & InputEvent.CTRL_MASK) == InputEvent.CTRL_MASK))
							{
								s.setSelection(false);
								/* et on l'enleve de la selection*/
								selection.removeSommet(s);
							}
						}
					}

					/* On parcours la liste d'arete*/
					for (Arete a : aretes) 
					{
						/* Si un arête touche le rectangle de selection alors elle devient sélectionnée */
						if(a.collisionRect(selection.x,selection.y,selection.width,selection.height) && a.isVisible())
						{
							a.setSelected(true);
							/*Et on l'ajoute à la selection */
							selection.addArete(a);
						}
						else
						{
							if (!((arg0.getModifiers() & InputEvent.CTRL_MASK) == InputEvent.CTRL_MASK))
							{
								a.setSelected(false);
								/* et on l'enleve de la selection*/
								selection.removeArete(a);
							}
						}
					}
					repaint();
				}
			}
		}
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	@Override
	public void mouseMoved(MouseEvent e) 
	{

	}

	/**
	 *Retourne le sommet qui contient la souris.
	 *Si aucun sommet ne contient la souris
	 *la méthode renvoie null
	 *  @param e
	 *  	L'evenement de la souris
	 */
	public Sommet SommetClicked(MouseEvent e)
	{
		for (Sommet s : this.getSommets()) 
		{
			if(s.containMouse(this.pointSouris(e).x,this.pointSouris(e).y) && s.getVisible())
			{
				return s;
			}
		}
		return null;
	}





	/**
	 *Cette m�thode est appel�e lorsque la souris
     est enfonc�e et qu'on la d�place sans relacher
	 *  @param arg0
	 *  	L'evenement de la souris
	 */


	/**
	 * Renvoi les coordonn�es de la souris
      sous forme de point en fonction du zoom
      et de l'origine du dessin
	 *  @param e
	 *  	L'evenement de la souris
	 *  @return le point de la souris
	 */
	public Point pointSouris(MouseEvent e)
	{
		return new Point((int)(e.getX()/zoom-origine.x),(int)(e.getY()/zoom-origine.y));
	}

	/**
    	Supprime tous les elements sélectionnés
	 **/
	public void supprimerElementsSelectionnes() {

		for (Sommet s : this.getSommets()) 
		{
			if(s.isSelected() == true)
			{
				s.setVisible(false);
			}
		}
		for (Arete a : this.getAretes()) 
		{
			if(a.isSelected() == true)
			{
				a.setVisible(false);
			}
		}
		repaint();
	}

	/**
	 * Retourne la taille des futurs sommets
	 * @return la taille des futurs sommets
	 */
	public int getTailleDefaut() {
		return tailleDefaut;
	}
	/**
	 * Affecte une nouvelle taille pour les futurs sommets
	 * @param tailleDefaut
	 * 			La nouvelle taille
	 */
	public void setTailleDefaut(int tailleDefaut) {
		this.tailleDefaut = tailleDefaut;
	}
	/**
	 * Retourne le zoom du graphe
	 * @return le zoom du graphe
	 */
	public float getZoom() {
		return zoom;
	}

	/**
	 * Affecte un nouveau zoom pour l'affichage du graphe
	 * @param d
	 * 			Le nouveau zoom
	 */
	public void setZoom(float d) {
		this.zoom = d;
	}

	/**
	 * Retourne le point d'origine du dessin du graphe
	 * @return le point d'origine du dessin du graphe
	 */
	public Point getOrigine() {
		return origine;
	}

	/**
	 * Affecte une nouvelle origine pour le dessin
       des �l�ments du graphe 
	 * @param origine
	 * 			La nouvelle origine
	 */
	public void setOrigine(Point origine) {
		this.origine = origine;
	}

	/**
	 * Retourne la couleur des futurs sommets
	 * @return la couleur des futurs sommets
	 */
	public Color getCouleurDefaut() {
		return couleurDefaut;
	}

	/**
	 * Affecte une nouvelle couleur pour les futurs
       sommets et ar�tes du graphe
	 * @param couleurDefaut
	 * 			La nouvelle couleur
	 */
	public void setCouleurDefaut(Color couleurDefaut) {
		this.couleurDefaut = couleurDefaut;
	}
	/**
	 * Retourne l'�paisseur des futurs sommets
	 * @return l'�paisseur des futurs sommets
	 */
	public int getEpaisseurDefaut() 
	{
		return epaisseurDefaut;
	}
	public void setEpaisseurDefaut(int epaisseurDefault) {
		this.epaisseurDefaut = epaisseurDefault;
	}

	/**
	 * Retourne la liste des sommets
	 * @return la liste des sommets
	 */
	public LinkedList<Sommet> getSommets() {
		return sommets;
	}

	/**
	 * Retourne la liste des ar�tes
	 * @return la liste des ar�tes
	 */
	public LinkedList<Arete> getAretes() {
		return aretes;
	}
	/**
	 * Affecte le nouveau type de sommet pour les futurs sommets
	 * @param typeSommet
	 * 			Le nouveau type de sommet
	 */
	public void setTypeSommet(String typeSommet) {
		this.typeSommet = typeSommet;
	}
	/**
	 * Change le mode (deplacement ou création)
	 * @param modeDeplacement
	 * 			Le nouveau mode
	 */
	public void setModeDeplacement(boolean modeDeplacement) {
		this.modeDeplacement = modeDeplacement;
	}
	
	
	void sauvegarder(){
		 try {
			 
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		 
				// root elements
				Document doc = docBuilder.newDocument();
				Element rootElement = doc.createElement("graphe");
				doc.appendChild(rootElement);
		 
				// staff elements
				for (Sommet s : sommets) {
					rootElement.appendChild(s.exportSommetXml(doc));
				}
				for (Arete a : aretes) {
					rootElement.appendChild(a.exportAreteXml(doc));
				}

				// write the content into xml file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				
				StreamResult result = new StreamResult(new File("Z:\\git\\projet_graphe\\sauvegarde.graphml"));
		 
				// Output to console for testing
				// StreamResult result = new StreamResult(System.out);
		 
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.transform(source, result);
		 
				System.out.println("File saved!");
		 
			  } catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			  } catch (TransformerException tfe) {
				tfe.printStackTrace();
			  }
		}
//	void importer()
//	{
//		SAXBuilder builder = new SAXBuilder();
//		  File xmlFile = new File("c:\\file.xml");
//	 
//		  try {
//	 
//			Document document = (Document) builder.build(xmlFile);
//			Element rootNode = document.getRootElement();
//			List list = rootNode.getChildren("staff");
//	 
//			for (int i = 0; i < list.size(); i++) {
//	 
//			   Element node = (Element) list.get(i);
//	 
//			   System.out.println("First Name : " + node.getChildText("firstname"));
//			   System.out.println("Last Name : " + node.getChildText("lastname"));
//			   System.out.println("Nick Name : " + node.getChildText("nickname"));
//			   System.out.println("Salary : " + node.getChildText("salary"));
//	 
//			}
//	 
//		  } catch (IOException io) {
//			System.out.println(io.getMessage());
//		  } catch (JDOMException jdomex) {
//			System.out.println(jdomex.getMessage());
//		  }
//		}
//	}
	
	

}
