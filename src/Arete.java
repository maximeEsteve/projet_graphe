import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
*
* <p>
Une ar�te est un trait qui relie deux sommets entre eux
* </p>

* Une ar�te est caract�ris�e par les informations suivantes :
 * <ul>
 * 		<li>Une couleur</li> 
 * 		<li>Une �paisseur</li>
 * 		<li>un sommet source</li>
 * 		<li>un sommet destination</li>
 * 
 * </ul>
* 
* @author Manu Patrois
* @version 1.0
* 
*/

public class Arete
{
	private Color couleur;
	private int epaisseur;
	private String etiquette;
	private Sommet source=null;
	private Sommet destination=null;
	private boolean visible;
	private boolean selected;
	
	private static int nombreArete=0;
	private int idArete;
	
	Arete(Color col,int epai,Sommet sour,Sommet dest)
	{
		nombreArete++;
		idArete=nombreArete;
		setCouleur(col);
		setEpaisseur(epai);
		setEtiquette("a"+idArete);
		setSource(sour);
		setDestination(dest);
		setVisible(true);
		setSelected(false);
	}
	
	Arete(Arete a,Sommet sour,Sommet dest)
	{
		nombreArete++;
		idArete=nombreArete;
		setCouleur(a.couleur);
		setEpaisseur(a.epaisseur);
		setEtiquette(a.getNom());
		setSource(sour);
		setDestination(dest);
		setVisible(true);
		setSelected(false);
	}
	
	
	/**
     * Trace l'ar�te sur la zone d'�dition
     * en fonction du zoom et de l'origine <br>
     * du dessin
     * @param g
     * 		La o� est dessin� l'ar�te
     * @param zoom
     * 		Le zoom du Graphe
     * @param origin
     * 		L' origine du Graphe
     */
	void tracerArete(Graphics g,float zoom,Point origin)
	{
		Graphics2D g2D=(Graphics2D)g;
		g2D.setStroke(new BasicStroke(getEpaisseur()));
		
		if(source.getVisible() && destination.getVisible() && isVisible())
		{
			if(selected)
				g.setColor(Color.RED);
			else
				g.setColor(couleur);
			
			g.drawLine((int)(source.centreSommet().x*zoom+origin.x*zoom),
				(int)(source.centreSommet().y*zoom+origin.y*zoom),
				(int)(destination.centreSommet().x*zoom+origin.x*zoom),
				(int)(destination.centreSommet().y*zoom+origin.y*zoom));
		
			g2D.setStroke(new BasicStroke(1));
			float x=((source.getPosX()*zoom+origin.x*zoom)+(destination.getPosX()*zoom+origin.x*zoom))/2;
			float y=((source.getPosY()*zoom+origin.y*zoom)+(destination.getPosY()*zoom+origin.y*zoom))/2;
			
			g2D.drawString(etiquette,x,y);
			
		}
		g2D.setStroke(new BasicStroke(1));
	}
//	public boolean containMouse(int coordX,int coordY)
//	{
//		
//	}
	
	/**
     * Retourne la couleur de l'ar�te
     * @return Couleur  de l'ar�te
     */
	public Color getCouleur() {
		return couleur;
	}
	
	/**
     * Affecte une nouvelle couleur � l'arete
     * @param couleur
     * 			La nouvelle couleur
     */
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}
	
	/**
     * Retourne l'�paisseur de l'ar�te
     * @return L'�paisseur de l'ar�te 
     */
	public int getEpaisseur() {
		return epaisseur;
	}
	
	/**
     * Affecte une nouvelle epaisseur au sommet
     * @param epaisseur
     * 			La nouvelle epaisseur
     */
	public void setEpaisseur(int epaisseur) {
		this.epaisseur = epaisseur;
	}

	/**
     * Retourne l' �tiquette du sommet
     * @return L'�tiquette du sommet
     */
	public String getNom() {
		return this.etiquette;
	}

	/**
     * Affecte une nouvelle �tiquette � l'arrete
     * @param nom
     * 			La nouvelle etiquette
     */
	public void setNom(String nom) {
		this.etiquette = nom;
	}
	void modifierEtiquetteArete()
	{
		
	}
	void modifierEpaisseur()
	{
		
	}
	void modifierCouleur()
	{
		
	}

//	public Color getCouleur() {
//		return couleur;
//	}
//
//	public void setCouleur(Color couleur) {
//		this.couleur = couleur;
//	}
//
//	public int getEpaisseur() {
//		return epaisseur;
//	}
//
//	public void setEpaisseur(int epaisseur) {
//		this.epaisseur = epaisseur;
//	}

	/**
     * Retourne l' �tiquette du sommet
     * @return L'�tiquette du sommet
     */
	public String getEtiquette() {
		return etiquette;
	}

	/**
     * Affecte une nouvelle �tiquette � l'arrete
     * @param etiquette
     * 			La nouvelle etiquette
     */
	public void setEtiquette(String etiquette) {
		this.etiquette = etiquette;
	}

	
	/**
    * Retourne le sommet source de l'arete
    * @return  Le sommet source de l'arete
    */
	public Sommet getSource() {
		return source;
	}

	/**
     * Affecte une nouvelle source � l'arete
     * @param source
     * 			La nouvelle source
     */
	public void setSource(Sommet source) {
		this.source = source;
	}

	/**
	    * Retourne le sommet destination de l'arete
	    * @return  Le sommet destination de l'arete
	    */
	public Sommet getDestination() {
		return destination;
	}

	/**
     * Affecte une nouvelle destination � l'arete
     * @param source
     * 			La nouvelle source
     */
	public void setDestination(Sommet destination) {
		this.destination = destination;
	}
	
	/**
     * Retourne si l'ar�te touche le rectangle pass� en
     * param�tre
     * @return Si l'ar�te touche le rectangle pass� en
     * param�tre
     */
	public boolean collisionRect(int xRect,int yRect,int wRect,int hRect) 
	{
		//Methode peut �tre lourde en calcul
		//On parcours tous les points de l'arete
		//et on verifie si il y en a un dans dans le rectangle
		//pass� en parametre
		
		
		// point de d�part
		int x1=source.centreSommet().x,y1=source.centreSommet().y;
		// point d'arriv�e
		int x2=destination.centreSommet().x,y2=destination.centreSommet().y;
		 
		// vecteur
		int dx=(x2-x1), dy=(y2-y1);
		int max = Math.max(Math.abs(dx), Math.abs(dy));
		
		// equation parametrique du segment
		//parcours de tous les points
		for(int t=0;t<=max;t++) 
		{
			int x = x1+(t*dx)/max;
			int y = y1+(t*dy)/max;
		 
			/* Si un point est dans le rectangle on retourne vrai*/
			if (x>=xRect && x <= (xRect+wRect)  &&
					y>=yRect && y <= (yRect+hRect) )
				return true;
			
		}
		return false;
	}
	


	/**
     * Défini si l'arrete est s�lectionn�e ou non
     * @param selected
     * 			La selection ou non de l'arete
     */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}


	/**
     * Retourn si l'arete est selectionnée ou non.
     * @return Si l'arete est selectionnée ou non.
     */
	public boolean isSelected() {
		return selected;
	}

	/**
     * Défini si l'arrete a été supprimer ou non
     * @param b
     * 			La visibilité ou non de l'arete
     */
	public void setVisible(boolean b) {
		visible=b;
	}


	public boolean isVisible() {
		return visible;
	}
	
	public String exportArete() {
		return ("		<edge id = "+'"'+ "e" + idArete + '"' + " source = " + '"'+ "n"+ getSource().getIdSommet() + '"' + 
				" destination = " + '"' + "n" + getDestination().getIdSommet() + '"'+ "/>");
	}
	public Element exportAreteXml(Document doc)
	{
		Element arete=doc.createElement("node");
		arete.setAttribute("id", idArete+"");
		arete.setAttribute("name", etiquette);
		arete.setAttribute("r", couleur.getRed()+"");
		arete.setAttribute("g", couleur.getGreen()+"");
		arete.setAttribute("b", couleur.getBlue()+"");
		arete.setAttribute("epaisseur",epaisseur+"");
		arete.setAttribute("source", source.getIdSommet()+"");
		arete.setAttribute("destination", destination.getIdSommet()+"");
		return arete;
	
	}
}
