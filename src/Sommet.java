import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * <p>
 *  Sommet est la classe représentant une forme soit :
 * <ul>
 * 		<li>Un rectangle</li> 
 * 		<li>Un rond</li>
 * 		<li>Un triangle</li>
 * </ul>
 * </p>
 * <p>
 * Un sommet est caractérisé par les informations suivantes :
 * <ul>
 * 		<li>Une couleur</li> 
 * 		<li>Une épaisseur</li>
 * 		<li>Un nom (ou étiquette )</li>
 * 		<li>Un id</li>
 * </ul>
 * La taille, la forme et la position du sommet sont contenus
 * dans l'attribut forme.
 * </p>
 * 
 * @see Rectangle
 * @see Rond
 * @see Triangle
 * @see Forme
 * 
 * @author Manu Patrois
 * @version 1.0
 * 
 */
public class Sommet 
{
	/**
	 * Le nombre du sommet.<br>
	 * Elle n'est pas définitive
	 */
	private Forme formeSommet=null;


	/**
	 * Le nombre de sommet.<br>
	 * Permet d'affecter un nouvel id chaque nouveau sommet 
	 */
	private static int nombreSommet=0;


	/**
	 * L'identifiant du sommet.<br>
	 * Deux sommets ne peuvent avoir le même id
	 */
	private final int idSommet;


	/**
	 * La couleur du sommet.<br>
	 * Elle n'est pas définitive
	 */
	private Color couleur;

	/**
	 * L'épaisseur des traits du sommet
	 * Elle n'est pas définitive
	 */
	private int epaisseur;

	/**
	 * Le nom ou étiquette du sommet.<br>
	 * Il n'est pas définitif
	 */
	private String nom;


	/**
	 * Si le sommet a été supprimer <br>
	 * 
	 */
	private boolean visible;

	/**
	 * Si le sommet a été selectionné <br>
	 * 
	 */
	private boolean selected;


	/**
	 * Instancie un nouveau sommet.
	 * 
	 * @param col
	 *            La couleur du sommet
	 * @param epai
	 * 			  L'épaisseur
	 * @param x
	 * 			  L'abscisse du sommet
	 * @param y
	 * 			  L'ordonn�e du sommet
	 * @param tail
	 * 			  La taille du sommet
	 * 
	 * @param typeS
	 * 			  La forme du sommet
	 */
	Sommet(Color col,int epai,int x,int y,int tail,String typeS)
	{
		nombreSommet++;
		idSommet=nombreSommet;
		couleur=col;
		epaisseur=epai;
		nom="n"+getIdSommet();
		visible=true;
		selected=false;

		if(typeS=="Rectangle")
			formeSommet=new Rectangle(x,y,tail);
		else if(typeS=="Rond")
			formeSommet=new Rond(x,y,tail);
		else if(typeS=="Triangle")
			formeSommet=new Triangle(x,y,tail);

	};
	
	
	Sommet(Sommet s)
	{
		nombreSommet++;
		idSommet=nombreSommet;
		couleur=s.couleur;
		epaisseur=s.epaisseur;
		nom=s.getNom();
		visible=true;
		selected=false;

		if(s.GetForme()=="Rectangle")
			formeSommet=new Rectangle(s.getPosX(),s.getPosY(),s.getTaille());
		else if(s.GetForme()=="Rond")
			formeSommet=new Rond(s.getPosX(),s.getPosY(),s.getTaille());
		else if(s.GetForme()=="Triangle")
			formeSommet=new Triangle(s.getPosX(),s.getPosY(),s.getTaille());
		
		//setForme(s.GetForme());

	};

	/**
	 * Déplace le sommet en fonction de x et y <br>
	 * @param x
	 * Déplace posX du sommet (de sa forme) en x
	 * @param y
	 * Déplace posY du sommet (de sa forme) en y
	 */
	public void move(int x,int y)
	{
		formeSommet.move(x, y);
	}
	
	public boolean pareil(Sommet somTest)
	{
		if( getPosX()==somTest.getPosX()
			&& getPosY()==somTest.getPosY()
			&& getTaille()==somTest.getTaille()
			&& getCouleur()==somTest.getCouleur()
			&& getEpaisseur()==somTest.getEpaisseur()
		)
			return true;
		else
			return false;
	}

	/**
	 * Trace le sommet sur la zone d'édition
	 * en fonction du zoom et de l'origine <br>
	 * du dessin
	 * @param g
	 * 		La où est dessiné le sommet
	 * @param zoom
	 * 		Le zoom du Graphe
	 * @param origin
	 * 		L' origine du Graphe
	 */
	void tracerSommet(Graphics g,float zoom,Point origin)
	{
		/*Si le sommet n'as pas été supprimé */

		if (this.getVisible())
		{
			/*On change l"épaisseur du trait */
			Graphics2D g2D=(Graphics2D)g;
			g2D.setStroke(new BasicStroke(getEpaisseur()));

			/*Si le sommet est selectionné on l'affiche rouge */
			if (this.isSelected())
			{
					g.setColor(Color.RED);
			}
			else
				g.setColor(getCouleur());


			/*Dessiner la forme du sommet */
			formeSommet.tracerSommet(g, zoom, origin);

			/*On remet l'épaisseur à une taille normale */
			g2D.setStroke(new BasicStroke(1));
			
			/*On dessine le nom du sommet */
			g2D.drawString(nom,formeSommet.getPosX()*zoom+origin.x*zoom, formeSommet.getPosY()*zoom+origin.y*zoom-2);
		}
	}


	/**
	 * Renvoi si les coordonnées coordX et coordY d'un point sont 
	 * contenues dans la forme du sommet.
	 * @param coordX
	 * 		Abscisse de la souris
	 * @param zoom
	 * 		Ordonn�e de la souris
	 * @return boolean
	 */
	public boolean containMouse(int coordX,int coordY)
	{
		return formeSommet.containMouse(coordX, coordY);
	}
	;

	/**
	 * Retourne le centre du sommet sous forme de point
	 * @return boolean
	 */
	public Point centreSommet(){
		return formeSommet.centreForme();
	}

	/**
	 * Retourne la couleur du sommet
	 * @return Couleur du sommet
	 */
	public Color getCouleur() {
		return couleur;
	}

	/**
	 * Affecte une nouvelle couleur au sommet
	 * @param couleur
	 * 			La nouvelle couleur
	 */
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}

	/**
	 * Retourne l'�paisseur du sommet
	 * @return L'�paisseur du sommet 
	 */
	public int getEpaisseur() {
		return epaisseur;
	}

	/**
	 * Affecte une nouvelle epaisseur au sommet
	 * @param epaisseur
	 * 			La nouvelle epaisseur
	 */
	public void setEpaisseur(int epaisseur) {
		this.epaisseur = epaisseur;
	}

	/**
	 * Retourne le nom du sommet
	 * @return Le nom du sommet
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Affecte un nouveau nom au sommet
	 * @param epaisseur
	 * 			La nouvelle epaisseur
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Retourne la position en abscisse du sommet
	 * @return int
	 */
	public int getPosX() {
		return formeSommet.getPosX();
	}


	/**
	 * Affecte une nouvelle position en abscisse
	 * @param posX
	 * 			La nouvelle position en abscisse
	 */
	public void setPosX(int posX) {
		formeSommet.setPosX(posX);
	}

	/**
	 * Retourne la position en ordonn�e du sommet
	 * @return La position en ordonn�e
	 */
	public int getPosY() {
		return formeSommet.getPosY();
	}

	/**
	 * Affecte une nouvelle position en ordonn�e
	 * @param posY
	 * 			La nouvelle position en ordonn�e
	 */
	public void setPosY(int posY) 
	{
		formeSommet.setPosY(posY);
	}

	/**
	 * Retourne la taille du sommet
	 * @return La taille du sommet
	 */
	public int getTaille() {
		return formeSommet.getTaille();
	}

	/**
	 * Affecte une nouvelle taille au sommet
	 * @param taille
	 * 			La taille du sommet
	 */
	public void setTaille(int taille) 
	{
		formeSommet.setTaille(taille);
	}

	/**
	 * Definit si le sommet est selectionn� ou pas
	 * @param b
	 * 			Si le sommet est selectionn� ou non
	 */
	public void setSelection(boolean b) {
		this.selected=b;

	}

	/**
	 * Retourne si le sommet est selectionn� ou pas
	 * @return Si le sommet est selectionn� ou pas
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Défini si le sommet a été selectionné
	 * @param b
	 * 			Si le sommet a été sélectionné ou non
	 */
	public void setVisible(boolean b) {
		this.visible=b;

	}

	/**
	 * Retourne si le sommet a �t� supprim� ou non 
	 * @return Si le sommet a �t� supprim� ou non 
	 */
	public boolean getVisible() {
		return this.visible;
	}

	/**
	 * Retourne si le sommet touche le rectangle pass� en
	 * paramètre
	 * @return Si le sommet touche un rectangle 
	 */
	public boolean InRectangle(int x,int y,int w,int h) 
	{
		return formeSommet.inRectangle(x,y,w,h);
	}

	/**
	 * Retourne si le sommet touche le rectangle pass� en
	 * paramètre
	 * @return Si le sommet touche un rectangle 
	 */
	public String GetForme()
	{
		return formeSommet.GetForme();
	};

	/**
	 * Affecte une nouvelle forme au sommet
	 */
	public void setForme(String typeS)
	{
		int x=formeSommet.getPosX();
		int y=formeSommet.getPosY();
		int tail=formeSommet.getTaille();

		formeSommet=null;
		if(typeS=="Rectangle")
			formeSommet=new Rectangle(x,y,tail);
		else if(typeS=="Rond")
			formeSommet=new Rond(x,y,tail);
		else if(typeS=="Triangle")
			formeSommet=new Triangle(x,y,tail);
	}
	
	public String exportSommet()
	{
		return ( "		<node id = "+'"'+ "n" + getIdSommet() + '"' + " shape =" + '"' + GetForme() + '"' + "/>");
	}
	
	public Element exportSommetXml(Document doc)
	{
		Element sommet=doc.createElement("node");
		sommet.setAttribute("id", idSommet+"");
		sommet.setAttribute("name", nom);
		sommet.setAttribute("shape", GetForme());
		sommet.setAttribute("taille", GetForme());
		sommet.setAttribute("r", couleur.getRed()+"");
		sommet.setAttribute("g", couleur.getGreen()+"");
		sommet.setAttribute("b", couleur.getBlue()+"");
		sommet.setAttribute("epaisseur",epaisseur+"");
		sommet.setAttribute("x", getPosX()+"");
		sommet.setAttribute("y", getPosY()+"");
		return sommet;
	
	}

	public int getIdSommet() {
		return idSommet;
	}
	
}
