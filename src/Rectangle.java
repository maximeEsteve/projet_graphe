import java.awt.Graphics;
import java.awt.Point;


/**
*
* <p>
*  Rectangle est une classe héritée de la classe forme
* </p>
* 
* @see Sommet
* @see Forme
* 
* @author Intelli'Graph
* @version 1.0
* 
*/
public class Rectangle extends Forme {

	/**
	 * Instancie un nouveau Rectangle.
     * @param x
     * 			  L'abscisse du Rectangle
     * @param y
     * 			  L'ordonnée du Rectangle
     * @param tail
     * 			  La taille du Rectangle
     */
	Rectangle(int x, int y, int tail) {
		super(x, y, tail);
	}

	
	@Override
	void tracerSommet(Graphics g, float zoom, Point origin) 
	{

			g.drawRect( (int)(getPosX()*zoom+origin.x*zoom),
						(int)(getPosY()*zoom+origin.y*zoom),
						(int)(getTaille()*zoom),
						(int)(getTaille()*zoom));
	}

	@Override
	public boolean containMouse(int coordX, int coordY)
	{
		if (coordX>this.getPosX() && coordX < (this.getPosX()+this.getTaille())  &&
				coordY>this.getPosY() && coordY < (this.getPosY()+this.getTaille()) )
			return true;
		else 
			return false;	
	}

	@Override
	public Point centreForme()
	{
		return new Point(this.getPosX()+this.getTaille()/2,this.getPosY()+this.getTaille()/2);
	}

	@Override
	public String GetForme() {
		return "Rectangle";
	}

	@Override
	boolean inRectangle(int x,int y,int w,int h) 
	{
		return !(x > getPosX()+getTaille() || 
		           x+w < getPosX() || 
		           y > getPosY()+getTaille() ||
		           y+h < getPosY());
	}

}

