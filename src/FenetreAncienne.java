//import java.awt.Adjustable;
//import java.awt.BorderLayout;
//import java.awt.Point;
//import java.awt.event.ActionEvent;
//import java.awt.event.AdjustmentEvent;
//import java.awt.event.AdjustmentListener;
//import java.awt.event.KeyEvent;
//
//import javax.swing.AbstractAction;
//import javax.swing.JButton;
//import javax.swing.JCheckBox;
//import javax.swing.JCheckBoxMenuItem;
//import javax.swing.JFrame;
//import javax.swing.JList;
//import javax.swing.JMenu;
//import javax.swing.JMenuBar;
//import javax.swing.JMenuItem;
//import javax.swing.JRadioButtonMenuItem;
//import javax.swing.JScrollBar;
//import javax.swing.JToolBar;
//import javax.swing.event.ListSelectionEvent;
//import javax.swing.event.ListSelectionListener;
//
//
//public class FenetrePrincipale extends JFrame implements AdjustmentListener 
//{
//	Graphe graphe=new Graphe();
//	JMenuBar menuBar;
//	JToolBar barreTest;
//	JMenu menu, submenu;
//	JMenuItem menuItem;
//	JRadioButtonMenuItem rbMenuItem;
//	JCheckBoxMenuItem cbMenuItem;
//	JScrollBar hori=new JScrollBar(Adjustable.HORIZONTAL,0,0,0,25);
//	JScrollBar verti=new JScrollBar(Adjustable.VERTICAL,0,0,0,25);
//	JList listForm;
//	JList listMode;
//	
//	FenetrePrincipale()
//	{
//		super("Dessin de Graphe");
//
//		this.setVisible(true);
//		
//		InitMenu();
//		pack();
//		this.setBounds(50,230,500,500);
//		setLayout(new BorderLayout());
//		add(hori,BorderLayout.SOUTH);
//		add(verti,BorderLayout.EAST);
//		barreTest=new JToolBar();
//		add(barreTest,BorderLayout.WEST);
//		verti.addAdjustmentListener(this);
//		verti.setMinimum(0);
//		verti.setMaximum(900);
//		hori.addAdjustmentListener(this);
//		hori.setMinimum(0);
//		hori.setMaximum(900);
//		this.add(graphe,BorderLayout.CENTER);
//	}
//	
//	
//	public void InitMenu()
//	{
//		menuBar = new JMenuBar();
//		
//		
//		menu = new JMenu("Fichier");
//		menuBar.add(menu);
//
//		menuItem = new JMenuItem(new MenuBarAction("Nouveau",KeyEvent.VK_N));
//	
//		
//		menu.add(menuItem);
//		menuItem = new JMenuItem(new MenuBarAction("Charger",KeyEvent.VK_C));
//		menu.add(menuItem);
//		
//		menu = new JMenu("Edition");
//		menuBar.add(menu);
//		menuItem = new JMenuItem("Copier");
//		menu.add(menuItem);
//		menuItem = new JMenuItem("Couper");
//		menu.add(menuItem);
//		menuItem = new JMenuItem("Coller");
//		menu.add(menuItem);
//		
//		JButton plus=new JButton(new MenuBarAction("+"));
//		menuBar.add(plus);
//		JButton moins=new JButton(new MenuBarAction("-"));
//		menuBar.add(moins);
//		JButton supprimer=new JButton(new MenuBarAction("Supprimer"));
//		menuBar.add(supprimer);
//		
//		
//		String[] selections = { "Rectangle","Rond","Triangle"};
//	    listForm = new JList(selections);
//	    listForm.setSelectedIndex(0);
//	    listForm.addListSelectionListener(new ListSelectionListener() {
//		        
//				public void valueChanged(ListSelectionEvent arg0) 
//				{
//					System.out.print(listForm.getSelectedValue().toString());
//					graphe.setTypeSommet(listForm.getSelectedValue().toString());
//				}
//					
//		      });
//	    
//	    menuBar.add(listForm);
//	    
//	    String[] modes = { "Cr�ation","D�placement","Selection"};
//	    listMode = new JList(modes);
//	    listMode.setSelectedIndex(0);
//	    listMode.addListSelectionListener(new ListSelectionListener() {
//		        
//				public void valueChanged(ListSelectionEvent arg0) 
//				{
//					//System.out.print(listMode.getSelectedValue().toString());
//					graphe.setMode(listMode.getSelectedValue().toString());
//				}
//					
//		      });
//	    
//	    menuBar.add(listMode);
//		
//		this.setJMenuBar(menuBar);
//	}
//	class MenuBarAction extends AbstractAction 
//	{
//		String name;
//	    public MenuBarAction(String text,Integer mnemonic) {
//	        super(text);
//	        name=text;
//	        putValue(MNEMONIC_KEY, mnemonic);
//	    }
//	    public MenuBarAction(String text) {
//	        super(text);
//	        name=text;
//	    }
//	    public void actionPerformed(ActionEvent e) 
//	    {
//	        if(name=="Nouveau")
//	        {
//	        	System.out.println("Nouveau � impl�menter ici");
//	        }
//	        else if(name=="Charger")
//	        {
//	        	System.out.println("Charger � impl�menter ici");
//	        }
//	        else if(name=="+")
//	        {
//	        	graphe.setZoom((float)(graphe.getZoom() * 1.1));
//	        }
//	        else if(name=="-")
//	        {
//	        	graphe.setZoom((float)(graphe.getZoom() / 1.1));
//	        }
//	        else if(name=="Supprimer")
//	        {
//	        	graphe.supprimerSommetSelectionne();
//	        }
//	        repaint();
//	    }
//	}
//	@Override
//	public void adjustmentValueChanged(AdjustmentEvent arg0) 
//	{
//			graphe.setOrigine(new Point(-hori.getValue(),-verti.getValue()));
//			repaint();
//	}
//	public static void main(String[] args) 
//	{
//		FenetrePrincipale fenetre=new FenetrePrincipale();
//		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//	}
//
//}
