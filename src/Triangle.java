import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;


/**
La classe triangle hérite de la classe forme.
 <p>
 * Le triangle est caractérisé par 3 points.
 * Le dessin du triangle est le tracé de lignes 
 * entre ses trois points.
 * 
 * @see Sommet
 * 
 * @author Intelli'Graph Team
 * @version 1.0
 */
public class Triangle extends Forme 
{

	Point haut;
	Point droite;
	Point gauche;
	
	/**
	 * Instancie un nouveau Triangle.
     * @param x
     * 			  L'abscisse du Triangle
     * @param y
     * 			  L'ordonnée du Triangle
     * @param tail
     * 			  La taille du Triangle
     */
	Triangle(int x, int y, int tail) 
	{
		super(x,y,tail);
		/* Créer trois point qui seront reliés ensemble lors du dessin et utilisé lors des calculs 
		 containsMouse, inRectangle ,move etc..*/
		haut=new Point(x,y-tail/2);
		droite=new Point(x+tail,y+tail);
		gauche=new Point(x-tail,y+tail);
	}

	@Override
	void tracerSommet(Graphics g, float zoom, Point origin) {

		/* Trace trois lignes qui relient les trois points */
		
			g.drawLine((int)(haut.x*zoom+origin.x*zoom),
						(int)(haut.y*zoom+origin.y*zoom),
						(int)(droite.x*zoom+origin.x*zoom),
						(int)(droite.y*zoom+origin.y*zoom));
			
			g.drawLine((int)(haut.x*zoom+origin.x*zoom),
					(int)(haut.y*zoom+origin.y*zoom),
					(int)(gauche.x*zoom+origin.x*zoom),
					(int)(gauche.y*zoom+origin.y*zoom));
			
			g.drawLine((int)(gauche.x*zoom+origin.x*zoom),
					(int)(gauche.y*zoom+origin.y*zoom),
					(int)(droite.x*zoom+origin.x*zoom),
					(int)(droite.y*zoom+origin.y*zoom));
		
	}

	@Override
	public boolean containMouse(int coordX, int coordY) {
		Polygon p=new Polygon();
		p.addPoint(haut.x,haut.y);
		p.addPoint(gauche.x,gauche.y);
		p.addPoint(droite.x,droite.y);
		return p.contains(new Point(coordX,coordY));
	}

	@Override
	public Point centreForme() {
		int x=(haut.x+gauche.x+droite.x)/3;
		int y=(haut.y+gauche.y+droite.y)/3;
		return new Point(x,y);
	}
	
	@Override
	public void move(int x,int y) 
	{
		setPosX(x);
		setPosY(y);
		haut=new Point(x,y-getTaille()/2);
		droite=new Point(x+getTaille(),y+getTaille());
		gauche=new Point(x-getTaille(),y+getTaille());
	}

	@Override
	public String GetForme() {
		return "Triangle";
	}

	@Override
	boolean inRectangle(int x, int y, int w, int h) {
		return !(x > getPosX()+getTaille() || 
		           x+w < getPosX() || 
		           y > getPosY()+getTaille() ||
		           y+h < getPosY());
	}

	
	

}
