import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;



import javax.swing.JColorChooser;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JSpinner;
import javax.swing.JToolBar;
import javax.swing.JComboBox;
import javax.swing.JButton;







import java.awt.event.ActionEvent;



import java.awt.event.ActionListener;
import java.awt.Button;



import java.awt.Dimension;
import java.awt.Label;
import java.awt.Point;

import javax.swing.JToggleButton;
import javax.swing.DefaultComboBoxModel;







import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;







import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
//import javax.swing.plaf.ColorChooserUI;

//import com.sun.org.apache.bcel.internal.generic.RETURN;

import java.awt.Color;
import javax.swing.SpinnerNumberModel;
import java.awt.Cursor;
//import javax.swing.JPopupMenu;
//import java.awt.Component;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import javax.swing.JSeparator;



public class FenetrePrincipale extends JFrame {

	private JPanel contentPane;
	Graphe graphe = new Graphe();
	JComboBox boxTypeSommet = new JComboBox();
	JButton btnSupprimer = new JButton("Supprimer");
	JButton buttonPlus = new JButton("+");
	JButton buttonMoins = new JButton("-");
	JToggleButton tglbtnDplacer = new JToggleButton("D\u00E9placer");
	private final Label label = new Label("Taille");
	private final Label label_1 = new Label("Epaisseur");
	JSpinner spinner = new JSpinner();
	JSpinner spinner_1 = new JSpinner();
	Button button = new Button("");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
					FenetrePrincipale frame = new FenetrePrincipale();
					frame.setVisible(true);
	}

	/**
	 * Create the frame.
	 */
	public FenetrePrincipale() {
		super("Graphe");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 817, 524);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setInheritsPopupMenu(true);
		menuBar.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		menuBar.setFocusTraversalPolicyProvider(true);
		menuBar.setFocusTraversalKeysEnabled(true);
		menuBar.setAutoscrolls(true);
		menuBar.setDoubleBuffered(true);
		menuBar.setFocusCycleRoot(true);
		setJMenuBar(menuBar);
		
		JMenu mnFichier = new JMenu("Fichier");
		mnFichier.setFocusCycleRoot(true);
		mnFichier.setBorderPainted(true);
		mnFichier.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		mnFichier.setFocusTraversalPolicyProvider(true);
		mnFichier.setFocusPainted(true);
		menuBar.add(mnFichier);
		
		JMenuItem mntmNouveau = new JMenuItem("Nouveau");
		mnFichier.add(mntmNouveau);
		
		JMenuItem mntmCharger = new JMenuItem("Charger");
		mnFichier.add(mntmCharger);
		
		JMenuItem mntmEnregistrer = new JMenuItem("Enregistrer");
		mntmEnregistrer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				graphe.sauvegarder();
			}
		});
		mnFichier.add(mntmEnregistrer);
		
		JMenuItem mntmEnregistrerSous = new JMenuItem("Enregistrer sous");
		mnFichier.add(mntmEnregistrerSous);
		
		JMenu mnEdition = new JMenu("Edition");
		menuBar.add(mnEdition);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		contentPane.setBackground(Color.LIGHT_GRAY);
		setContentPane(contentPane);
		graphe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		graphe.setBounds(10, 36, 781, 418);
		contentPane.setLayout(null);
		contentPane.add(graphe);
		
		GroupLayout gl_graphe = new GroupLayout(graphe);
		gl_graphe.setHorizontalGroup(
			gl_graphe.createParallelGroup(Alignment.LEADING)
				.addGap(0, 766, Short.MAX_VALUE)
		);
		gl_graphe.setVerticalGroup(
			gl_graphe.createParallelGroup(Alignment.LEADING)
				.addGap(0, 361, Short.MAX_VALUE)
		);
		graphe.setLayout(gl_graphe);
		
		JToolBar toolBar_1 = new JToolBar();
		toolBar_1.setBounds(0, 0, 521, 25);
		contentPane.add(toolBar_1);
		toolBar_1.add(buttonPlus);
		toolBar_1.add(buttonMoins);
		toolBar_1.add(boxTypeSommet);
		boxTypeSommet.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				graphe.setTypeSommet(boxTypeSommet.getSelectedItem().toString());
				
			}
		});
		
		boxTypeSommet.setModel(new DefaultComboBoxModel(new String[] {"Rectangle", "Rond", "Triangle"}));
		toolBar_1.add(label);
		
		
		spinner_1.setValue(graphe.getTailleDefaut());
		spinner_1.setMinimumSize(new Dimension(40, 20));
		toolBar_1.add(spinner_1);
		toolBar_1.add(label_1);
		
		
		spinner_1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				
				if((int) spinner_1.getValue()<1)
					spinner_1.setValue(1);
				graphe.setTailleDefaut((int) spinner_1.getValue());
			}
		});
		spinner.setModel(new SpinnerNumberModel(new Integer(1), null, null, new Integer(1)));
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if((int) spinner.getValue()<1)
					spinner.setValue(1);
				graphe.setEpaisseurDefaut((int) spinner.getValue());
			}
		});
		spinner.setMinimumSize(new Dimension(40, 20));
		toolBar_1.add(spinner);
		toolBar_1.add(tglbtnDplacer);
		tglbtnDplacer.setActionCommand("D\u00E9placer");
		toolBar_1.add(btnSupprimer);
		toolBar_1.add(button);
		
		button.setBackground(Color.BLACK);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				Color c = JColorChooser.showDialog(null, "Choose a Color", button.getForeground());
				if(c!=null)
				{
					button.setBackground(c);
					graphe.setCouleurDefaut(c);
				}
			}
		});
		
		
		btnSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				graphe.supprimerElementsSelectionnes();
			}
		});
		
		
		tglbtnDplacer.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) 
			{
				graphe.setModeDeplacement(tglbtnDplacer.isSelected());
			}
		});
		
		
		buttonMoins.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				graphe.setZoom((float)(graphe.getZoom() / 1.1));
				System.out.println(graphe.getOrigine());
				repaint();
			}
		});
		
		
		buttonPlus.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) {
				graphe.setZoom((float)(graphe.getZoom() * 1.1));
				repaint();
			
			}
		});
	}
}
