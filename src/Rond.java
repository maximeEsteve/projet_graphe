import java.awt.Graphics;
import java.awt.Point;


/**
*
* <p>
*  Sommet est une classe héritée de la classe Rectangle
*  car seul la fonction de dessine change,ils ont exactement
*  les mêmes attributs.
* </p>
* 
* @see Sommet
* @see Rectangle
* 
* @author Intelli'Graph team
* @version 1.0
* 
*/

public class Rond extends Rectangle
{
	/**
	 * Instancie un nouveau Rond.
     * 
     * @param x
     * 			  L'abscisse du Rond
     * @param y
     * 			  L'ordonnée du Rond
     * @param tail
     * 			  La taille du Rond
     */
	Rond(int x, int y, int tail) {
		super(x, y, tail);
	}

	@Override
	void tracerSommet(Graphics g, float zoom, Point origin) 
	{
			g.drawOval( (int)(getPosX()*zoom+origin.x*zoom),
						(int)(getPosY()*zoom+origin.y*zoom),
						(int)(getTaille()*zoom),
						(int)(getTaille()*zoom));
	}
	@Override
	public String GetForme() {
		return "Rond";
	}

	
}
