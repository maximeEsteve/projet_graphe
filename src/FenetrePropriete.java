import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JColorChooser;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import sun.reflect.generics.tree.ReturnType;

import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


public class FenetrePropriete extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JLabel lblEpaisseur;
	JSpinner spinnerEpaisseur = new JSpinner();
	JButton buttonCouleur = new JButton("");
	JSpinner spinnerTaille = new JSpinner();
	JComboBox comboBoxForme = new JComboBox();
	JTextField textFieldEtiquette = new JTextField();
	JLabel lblTaille = new JLabel("Taille");
	JLabel lblCouleur = new JLabel("Couleur");
	JLabel lblEtiquette = new JLabel("Etiquette");
	JLabel lblForme = new JLabel("Forme");
	LinkedList<Sommet> listSommet;
	LinkedList<Arete> listArete;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the dialog.
	 */
	public FenetrePropriete(LinkedList<Sommet> listSommet1,LinkedList<Arete> listArete1) 
	{
		super();
		listSommet=listSommet1;
		listArete=listArete1;
		
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			lblEpaisseur = new JLabel("Epaisseur");
		}
		
		
		buttonCouleur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color c = JColorChooser.showDialog(null, "Choose a Color", buttonCouleur.getForeground());
				if(c!=null)
				{
					buttonCouleur.setBackground(c);
				}
			}
		});

		buttonCouleur.setActionCommand("OK");
		
		textFieldEtiquette.setColumns(10);
		comboBoxForme.setModel(new DefaultComboBoxModel(new String[] {"Rectangle", "Rond", "Triangle"}));
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(31)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(lblCouleur, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(buttonCouleur, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblEpaisseur)
								.addComponent(lblTaille))
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addGap(28)
									.addComponent(spinnerEpaisseur, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPanel.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(spinnerTaille, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addGap(94)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblForme, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblEtiquette, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE))))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(textFieldEtiquette, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBoxForme, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(24))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEpaisseur)
						.addComponent(spinnerEpaisseur, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldEtiquette, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblEtiquette))
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(34)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(comboBoxForme, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblForme)))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(18)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(buttonCouleur, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblCouleur))
							.addGap(18)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(spinnerTaille, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblTaille))))
					.addContainerGap(114, Short.MAX_VALUE))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
//						sommetPropr=new Sommet(buttonCouleur.getBackground(),(int) spinnerEpaisseur.getValue(), 
//							0, 0, (int)spinnerTaille.getValue(),(String) comboBoxForme.getSelectedItem());
//						sommetPropr.setNom(textFieldEtiquette.getText());
						
						if(!lblForme.isVisible() && !lblEtiquette.isVisible())
						{
							for (Arete a : listArete) 
							{
								a.setCouleur(buttonCouleur.getBackground());
								a.setEpaisseur((int) spinnerEpaisseur.getValue());
							}
							for (Sommet s : listSommet) 
							{
								s.setCouleur(buttonCouleur.getBackground());
								s.setEpaisseur((int) spinnerEpaisseur.getValue());
							}
						}
						else if(lblForme.isVisible() && !lblEtiquette.isVisible())
						{
							for (Sommet s : listSommet) 
							{
								s.setCouleur(buttonCouleur.getBackground());
								s.setEpaisseur((int) spinnerEpaisseur.getValue());
								s.setForme((String) comboBoxForme.getSelectedItem());
								s.setTaille((int)spinnerTaille.getValue());
							}
						}
						else if (lblForme.isVisible() && lblEtiquette.isVisible())
						{
							for (Sommet s : listSommet) 
							{
								s.setCouleur(buttonCouleur.getBackground());
								s.setEpaisseur((int) spinnerEpaisseur.getValue());
								s.setForme((String) comboBoxForme.getSelectedItem());
								s.setTaille((int)spinnerTaille.getValue());
								s.setNom(textFieldEtiquette.getText());
							}
						}
						else if (!lblForme.isVisible() && lblEtiquette.isVisible())
						{
							for (Arete a : listArete) 
							{
								a.setCouleur(buttonCouleur.getBackground());
								a.setEpaisseur((int) spinnerEpaisseur.getValue());
								a.setNom(textFieldEtiquette.getText());
							}
						}
						
						
						
						
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//sommetPropr=null;
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
//			textFielEtiquette.
			
		}
		
		
		spinnerTaille.setValue(20);
		spinnerEpaisseur.setValue(1);
		buttonCouleur.setBackground(Color.black);
		comboBoxForme.setSelectedItem("Rectangle");
		
		if( (listSommet.size()>0 && listArete.size()>0  ))
		{
			lblForme.setVisible(false);
			lblEtiquette.setVisible(false);
			lblTaille.setVisible(false);
			comboBoxForme.setVisible(false);
			textFieldEtiquette.setVisible(false);
			spinnerTaille.setVisible(false);
//			if(listSommet.size()==0)
//			this.setTitle("Modifier Arete(s)");
//			else
//			this.setTitle("Modifier aretes et sommets");
		}
		else if (listSommet.size()>1 && listArete.size()==0)
		{
			textFieldEtiquette.setVisible(false);
			lblEtiquette.setVisible(false);
			this.setTitle("Modifier les sommets");
			
			
			Sommet tmp=listSommet.getLast();
			
			comboBoxForme.setSelectedItem(tmp.GetForme());
			System.out.println(tmp.GetForme());
			spinnerTaille.setValue(tmp.getTaille());
			spinnerEpaisseur.setValue(tmp.getEpaisseur());
			buttonCouleur.setBackground(tmp.getCouleur());
			comboBoxForme.setSelectedItem(tmp.GetForme());
		}
		else if (listSommet.size()==1 && listArete.size()==0)
		{
			Sommet tmp=listSommet.getLast();
			textFieldEtiquette.setText(tmp.getNom());
			spinnerTaille.setValue(tmp.getTaille());
			spinnerEpaisseur.setValue(tmp.getEpaisseur());
			buttonCouleur.setBackground(tmp.getCouleur());
			comboBoxForme.setSelectedItem(tmp.GetForme());
			
			
			this.setTitle("Modifier le sommet");
			
		}
		else if (listSommet.size()==0 && listArete.size()==1)
		{
			lblForme.setVisible(false);
			comboBoxForme.setVisible(false);
			
			Arete tmp=listArete.getLast();
			textFieldEtiquette.setText(tmp.getNom());
			spinnerEpaisseur.setValue(tmp.getEpaisseur());
			buttonCouleur.setBackground(tmp.getCouleur());
			
			
			this.setTitle("Modifier l'arete");
			
			
		}
		
		
	}
	void showDialog() {
	    setVisible(true);
	}
}
